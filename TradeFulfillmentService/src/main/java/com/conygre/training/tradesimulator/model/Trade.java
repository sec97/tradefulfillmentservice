package com.conygre.training.tradesimulator.model;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;
    private LocalDateTime timeStamp;
    private String stockTicker;
    private String side;
    private double stockQuantity;
    private double stockPrice;
    private double totalPrice = stockQuantity * stockPrice;
    private String tradeStatus;

    public Trade(ObjectId id, String stockTicker, String side, double stockQuantity, double stockPrice,
    String tradeStatus) {
        this.id = id;
        this.stockTicker = stockTicker;
        this.side = side;
        this.stockQuantity = stockQuantity;
        this.stockPrice = stockPrice;
        this.tradeStatus = tradeStatus;
    }

    public Trade() {
        
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public double getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(double stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public double getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(double stockPrice) {
        this.stockPrice = stockPrice;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    
}